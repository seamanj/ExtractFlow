import argparse
import sys
import os
import os.path as osp
import glob
from pipes import quote
from multiprocessing import Pool, current_process

import mmcv

def dump_frames(full_path, params):

    vid_path = full_path.split('/')[-1]
    vid_name = vid_path[:vid_path.rindex('.')]
    out_full_path = osp.join(params.output_root, vid_name)
    try:
        os.mkdir(out_full_path)
    except OSError:
        pass
    vr = mmcv.VideoReader(full_path)
    for i in range(len(vr)):
        if vr[i] is not None:
            mmcv.imwrite(
                vr[i], '{}/img_{:05d}.jpg'.format(out_full_path, i + 1))
        else:
            print('[Warning] length inconsistent!'
                  'Early stop with {} out of {} frames'.format(i + 1, len(vr)))
            break
    print('{} done with {} frames'.format(vid_name, len(vr)))
    sys.stdout.flush()
    return True


def run_optical_flow(full_path, params, dev_id=0):
    vid_path = full_path.split('/')[-1]
    vid_name = vid_path[:vid_path.rindex('.')]
    out_full_path = osp.join(params.output_root, vid_name)
    try:
        os.mkdir(out_full_path)
    except OSError:
        pass


    image_path = '{}/img'.format(out_full_path)
    flow_x_path = '{}/flow_x'.format(out_full_path)
    flow_y_path = '{}/flow_y'.format(out_full_path)

    cmd = osp.join(params.df_path, 'build/extract_gpu') + \
          ' -f={} -x={} -y={} -i={} -b=20 -t=1 -d={} -s=1 -o={} -w={} -h={}' \
              .format(
              quote(full_path),
              quote(flow_x_path), quote(flow_y_path), quote(image_path),
              dev_id, params.out_format, params.new_width, params.new_height)

    os.system(cmd)
    print('{} done'.format( vid_name))
    sys.stdout.flush()
    return True


def run_warp_optical_flow(full_path, params, dev_id=0):
    vid_path = full_path.split('/')[-1]
    vid_name = vid_path[:vid_path.rindex('.')]

    vid_name = vid_path[:vid_path.rindex('.')]
    out_full_path = osp.join(params.output_root, vid_name)
    try:
        os.mkdir(out_full_path)
    except OSError:
        pass

    flow_x_path = '{}/flow_x'.format(out_full_path)
    flow_y_path = '{}/flow_y'.format(out_full_path)

    cmd = osp.join(params.df_path, 'build/extract_gpu') + \
          ' -f={} -x={} -y={} -b=20 -t=1 -d={} -s=1 -o={}'.format(
            quote(full_path), quote(flow_x_path), quote(flow_y_path),
            dev_id, params.out_format)


    os.system(cmd)
    print('warp on {} done'.format(vid_name))
    sys.stdout.flush()
    return True


def main(params):

    video_path = params.video_path
    output_root = params.output_root

    if params.flow_type == 'tvl1': # tj : denseflow 会从第2帧开始写数据， 所以图片和flow是一样的数量， 第一帧的图片被省略了
        run_optical_flow(params.video_path, params)
    elif params.flow_type == 'warp_tvl1':
        run_warp_optical_flow(params.video_path, params)
    else:
        dump_frames(params.video_path, params)

def load_pt():
    a = torch.load("/vol/research/extol/tmp/Tao_MASAL_New/EfficientNet7/test/ln-1911.lb-580.gl-junior.pr-26.vd-YrBEWpOlFJQ.en7.pt")
    print(1)

if __name__ == "__main__":


    # load_pt()

    parser = argparse.ArgumentParser()

    parser.add_argument("--video_path", type=str, default=None, help="")
    parser.add_argument("--output_root", type=str, default=None, help="")
    parser.add_argument('--df_path', type=str,
                        default='/opt/dense_flow')
    parser.add_argument('--flow_type', type=str,
                        default='tvl1', choices=[None, 'tvl1', 'warp_tvl1'])
    parser.add_argument("--out_format", type=str, default='dir',
                        choices=['dir', 'zip'], help='output format')
    parser.add_argument("--new_width", type=int, default=0,
                        help='resize image width')
    parser.add_argument("--new_height", type=int,
                        default=0, help='resize image height')
    params, _ = parser.parse_known_args()
    main(params)
