import os
import argparse
import subprocess
import torch


def main(params):

    input_dir = params.input_dir
    output_dir = params.output_dir


    videos = [
        {
            "file": y,
            "file_name": ".".join(y.split(".")[:-1]),
            "input_file": os.path.join(p, y),
            #"output_file": ".".join(y.split(".")[:-1]) + ".mp4",
            # "output_folder": os.path.join(output_dir, os.path.relpath(p, input_dir)), # tj : has subdir
            "output_folder": os.path.join(output_dir, ".".join(y.split(".")[:-1]) ),  # tj : has no subdir

        }
        for p, _, x in os.walk(input_dir)
        for y in x
        if y.split(".")[-1] == "mp4"
    ]
    i = 0
    # for v in videos:
    #     if not os.path.exists(v["output_folder"]):
    #         os.makedirs(v["output_folder"])
    with open(params.base_submit_filename, "r") as f:
        contents = f.read()
        with open(params.submit_file, "w+") as f1:
            f1.write(contents)
            f1.write("\n\n")
            for v in videos:
                of = os.path.join(v["output_folder"])
                if not os.path.exists(of):
                    i += 1
                    f1.write("queue 1 Script matching files ({}) \n\n".format(v["input_file"]))
                    f1.write("# ----------------------------------------------------------------------------------------------------------------------")
                    f1.write("\n\n")
    print("{} new jobs are created".format(i))



if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--input_dir", type=str, default="/vol/research/extol/tmp/Tao_MASAL_New/output_224x224_video/train", help="")
    parser.add_argument("--output_dir", type=str, default="/vol/research/extol/tmp/Tao_MASAL_New/output_224x224_video/rawframes/train", help="")
    parser.add_argument(
        "--base_submit_filename",
        type=str,
        default="./base.submit_file",
        help="",
    )
    parser.add_argument("--submit_file", type=str, default="./append_train.submit_file", help="")
    params, _ = parser.parse_known_args()
    main(params)
