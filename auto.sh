#!/bin/bash




input_file=$1
sub_dir=$2
output_root="/vol/research/extol/tmp/Tao_MASAL_New/output_224x224_video/rawframes/"$sub_dir

filename=${input_file##*/}
filename=${filename%.*}

if [ ! -d $output_root ]; then
  echo "create output folder $output_root"
  mkdir -p $output_root
fi


echo "start extract frames for $filename"
cd /vol/research/SignPose/tj/Workspace/ExtractFlow
/vol/research/SignPose/tj/Software/miniconda3/envs/BERT_skeleton/bin/python extract_frames.py --video_path $1 --output_root $output_root
