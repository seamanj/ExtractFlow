import os
import argparse
import subprocess
import torch


def main(params):

    input_dir = params.input_dir
    output_dir = params.output_dir
    output_suffix = params.output_suffix

    videos = [
        {
            "file": y,
            "file_name": ".".join(y.split(".")[:-1]),
            "input_file": os.path.join(p, y),
            "output_file": ".".join(y.split(".")[:-1]) + "." + output_suffix + ".pt",
            # "output_folder": os.path.join(output_dir, os.path.relpath(p, input_dir)), # tj : has subdir
            "output_folder": output_dir,  # tj : has no subdir

        }
        for p, _, x in os.walk(input_dir)
        for y in x
        if y.split(".")[-1] == "mp4"
    ]

    for v in videos:
        if not os.path.exists(v["output_folder"]):
            os.makedirs(v["output_folder"])
    with open(params.base_submit_filename, "r") as f:
        contents = f.read()
        with open(params.submit_file, "w+") as f1:
            f1.write(contents)
            f1.write("\n\n")
            i = 0
            for v in videos:
                of = os.path.join(v["output_folder"], v["output_file"])
                if not os.path.exists(of):
                    f1.write("video_path = --video_path {}\n".format(v["input_file"]))
                    f1.write("output_path = --output_path {}\n".format(of))
                    f1.write("arguments = $(script) $(model) $(video_path) $(output_path)\n")
                    f1.write("queue 1\n\n")
                    f1.write("# ----------------------------------------------------------------------------------------------------------------------")
                    f1.write("\n\n")
                # else:
                #     ffmpeg_cmd = (
                #         "ffprobe "
                #         + "-v error "
                #         + "-select_streams v:0 "
                #         + "-show_entries stream=nb_frames "
                #         + "-of default=nokey=1:noprint_wrappers=1 "
                #         + v["input_file"]
                #     )
                #     num_frames = int(subprocess.getoutput(ffmpeg_cmd))
                #     num_features = torch.load(of).shape[0]
                #     if num_frames != num_features:
                #         print(
                #             "Removed: Images: "
                #             + str(num_features)
                #             + " Frames: "
                #             + str(num_frames)
                #             + " => "
                #             + of
                #         )
                #     else:
                #         i = i + 1
                #         print(
                #             "["
                #             + str(i)
                #             + "] Images: "
                #             + str(num_features)
                #             + " Frames: "
                #             + str(num_frames)
                #             + " => "
                #             + of
                #         )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--input_dir", type=str, default="/vol/research/extol/tmp/Tao_MASAL_New/output_cropped_video/test", help="")
    parser.add_argument("--output_dir", type=str, default="/vol/research/extol/tmp/Tao_MASAL_New/EfficientNet7/test", help="")
    parser.add_argument("--output_suffix", type=str, default="en7", help="")
    parser.add_argument(
        "--base_submit_filename",
        type=str,
        default="/vol/research/SignPose/tj/Workspace/ExtractCNNFeatures/submit_files/base.en7.submit_file",
        help="",
    )
    parser.add_argument("--submit_file", type=str, default="./run_test.submit_file", help="")
    params, _ = parser.parse_known_args()
    main(params)
