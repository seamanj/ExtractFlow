import argparse
import os.path as osp
import glob
import os
import fnmatch
import re
from natsort import natsorted, ns

def parse_args():
    parser = argparse.ArgumentParser(description='Build file list')

    parser.add_argument('--frame_path', type=str, default='/vol/research/extol/tmp/Tao_MASAL_New/output_224x224_video/rawframes/train',
                        help='root directory for the frames')
    parser.add_argument('--rgb_prefix', type=str, default='img_')
    parser.add_argument('--flow_x_prefix', type=str, default='flow_x_')
    parser.add_argument('--flow_y_prefix', type=str, default='flow_y_')
    parser.add_argument('--level', type=int, default=1, choices=[1, 2])
    parser.add_argument('--format', type=str,
                        default='rawframes', choices=['rawframes', 'videos'])
    parser.add_argument('--out_list_path', type=str, default='/vol/research/extol/tmp/Tao_MASAL_New/output_224x224_video/rawframes/train.txt')
    parser.add_argument('--shuffle', action='store_true', default=False)
    args = parser.parse_args()

    return args


def parse_directory(path, key_func=lambda x: x[-11:],
                    rgb_prefix='img_',
                    flow_x_prefix='flow_x_',
                    flow_y_prefix='flow_y_',
                    level=1):
    """
    Parse directories holding extracted frames from standard benchmarks
    """
    print('parse frames under folder {}'.format(path))
    if level == 1:
        frame_folders = glob.glob(os.path.join(path, '*'))
    elif level == 2:
        frame_folders = glob.glob(os.path.join(path, '*', '*'))
    else:
        raise ValueError('level can be only 1 or 2')

    def count_files(directory, prefix_list):
        lst = os.listdir(directory)
        cnt_list = [len(fnmatch.filter(lst, x+'*')) for x in prefix_list]
        return cnt_list

    # check RGB
    frame_dict = {}
    for i, f in enumerate(frame_folders):
        all_cnt = count_files(f, (rgb_prefix, flow_x_prefix, flow_y_prefix))
        k = key_func(f)

        x_cnt = all_cnt[1]
        y_cnt = all_cnt[2]
        if x_cnt != y_cnt:
            raise ValueError(
                'x and y direction have different number '
                'of flow images. video: ' + f)
        # if i % 200 == 0:
        #     print('{} videos parsed'.format(i))

        frame_dict[k] = (f, all_cnt[0], x_cnt)
        print('{} videos parsed'.format(i+1))
    print('frame folder analysis done')
    return frame_dict



def main():
    args = parse_args()

    if args.level == 2:
        def key_func(x): return '/'.join(x.split('/')[-2:])
    else:
        def key_func(x): return x.split('/')[-1]

    if args.format == 'rawframes':
        frame_info = parse_directory(args.frame_path,
                                     key_func=key_func,
                                     rgb_prefix=args.rgb_prefix,
                                     flow_x_prefix=args.flow_x_prefix,
                                     flow_y_prefix=args.flow_y_prefix,
                                     level=args.level)
    else:
        if args.level == 1:
            video_list = glob.glob(osp.join(args.frame_path, '*'))
        elif args.level == 2:
            video_list = glob.glob(osp.join(args.frame_path, '*', '*'))
        frame_info = {osp.relpath(
            x.split('.')[0], args.frame_path): (x, -1, -1) for x in video_list}


    out_path = args.out_list_path

    # tj = : https://stackoverflow.com/questions/4836710/is-there-a-built-in-function-for-string-natural-sort
    frame_list = natsorted(frame_info.items())

    # frame_list = natsorted(x, key=lambda y: y.lower())



    with open(out_path, 'w') as f:
        for i in range(len(frame_list)):
            dir_path = frame_list[i][1][0]   # 1 is the tuple, 0 is the name in the tuple'/home/seamanj/Workspace/Skeletor/data/output_224x224_video/rawframes/train_10/ln-0.lb-830.gl-match.pr-0.vd-C37R_Ix8-qs'
            vid = '/'.join(dir_path.split('/')[-2:]) # 'train_10/ln-0.lb-830.gl-match.pr-0.vd-C37R_Ix8-qs'
            num_frames = frame_list[i][1][2]
            label = int(re.search("(?<=lb-)\d+(?=.*)", vid)[0]) # tj : 0 get the result
            f.writelines("{} {} {}\n".format(vid, num_frames, label))


if __name__ == "__main__":
    main()
